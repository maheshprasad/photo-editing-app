package com.example.maheshprasad.photoeditingapp.Interface;

import com.zomato.photofilters.imageprocessors.Filter;

public interface FiltersListFragmentListener {
    void onFiltereSelected(Filter filter);
}
