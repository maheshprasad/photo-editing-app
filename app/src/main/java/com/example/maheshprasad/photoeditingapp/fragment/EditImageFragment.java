package com.example.maheshprasad.photoeditingapp.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import com.example.maheshprasad.photoeditingapp.Interface.EditImageFragmentListener;
import com.example.maheshprasad.photoeditingapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditImageFragment extends Fragment implements SeekBar.OnSeekBarChangeListener {

    private EditImageFragmentListener listener;
    SeekBar seekbar_brightness, seekbar_constrant, seekbar_saturation;

    public EditImageFragment() {
        // Required empty public constructor
    }


    public void setListener(EditImageFragmentListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_image, container, false);
        seekbar_brightness = view.findViewById(R.id.seekbar_brightness);
        seekbar_constrant = view.findViewById(R.id.seekbar_constraint);
        seekbar_saturation = view.findViewById(R.id.seekbar_saturation);

        //Brightness
        seekbar_brightness.setMax(200);
        seekbar_brightness.setProgress(100);


        //Saturation
        seekbar_saturation.setMax(200);
        seekbar_saturation.setProgress(100);

        //Constrant
        seekbar_constrant.setMax(200);
        seekbar_constrant.setProgress(100);

        seekbar_brightness.setOnSeekBarChangeListener(this);
        seekbar_constrant.setOnSeekBarChangeListener(this);
        seekbar_saturation.setOnSeekBarChangeListener(this);

        return view;

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (listener !=null)
            {
                if (seekBar.getId() == R.id.seekbar_brightness){
                    listener.onBrightnessChanged(progress-100);
                }else if (seekBar.getId() == R.id.seekbar_constraint){
                    progress += 10;
                    float value = .10f*progress;
                    listener.onConstrantChanged(value);
                }else if (seekBar.getId() == R.id.seekbar_saturation){
                    float value = .10f*progress;
                    listener.onSaturationChanged(value);
                }
            }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
            if (listener!=null)
                listener.onEditStarted();
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
            if (listener !=null)
                listener.onEditCompleted();
    }

    public void resetControls(){
        seekbar_saturation.setProgress(10);
        seekbar_constrant.setProgress(0);
        seekbar_brightness.setProgress(100);
    }

}
